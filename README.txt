------------------------------------------------------------------------------
  User Badges Taxonomy Module
------------------------------------------------------------------------------

Description
===========
The User Badges Taxonomy was created for and is actively used on <a href="https://community.norton.com" title="Norton Community">Norton Community</a> Drupal 7 site. The Norton Community needed a fieldable and expandable User Badge functionality and the existing User Badges module (<a href="https://www.drupal.org/project/user_badges">User Badges</a>) could not fulfill these requirements. Therefore we have decided to employ fieldable Taxonomy Term entity to implement functionality of customizable user badges based on its own vocabulary. 

The module does the following upon installation:

1. Creates a new 'User Badges' vocabulary (machine name 'user_badges') with a number of default badges;

2. Creates a number of default roles - 'Employee', 'Partner', 'Technical Support', 'Trusted Advisor' and maps them to default badges;

3. Creates a new 'taxonomy_term_reference' field for the user entity to store user badges;

4. Processes existing users - assigns them badges based on their roles.

The module provides a configuration form to associate any user role to a badge in 'User Badges' vocabulary. 

If the mapping changes, the module updates all users that receive new, change existing or lose a badge.

If a role has been deleted, the module employs hook_user_role_delete() to update user badges for all affected users.

The module employs Rules to assign/reassign badges on 'user_insert' and 'user_update' events. 

Development supported by Symantec Corporation.

