<?php

/**
 * @file
 * Rules integration for user_badges_taxonomy module.
 */

/**
 * Implements hook_rules_condition_info().
 */
function user_badges_taxonomy_rules_condition_info() {
  $items = array();
  $items['user_badges_taxonomy_user_qualifies_for_badge'] = array(
    'label' => t('User qualifies for a Badge'),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
      ),
    ),
    'group' => t('User'),
    'callbacks' => array(
      'execute' => 'user_badges_taxonomy_user_qualifies_for_badge',
    ),
  );

  $items['user_badges_taxonomy_user_badge_qualifications_changed'] = array(
    'label' => t('User roles changed'),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('Updated User'),
      ),
      'account_unchanged' => array(
        'type' => 'user',
        'label' => t('Unchanged User'),
      ),
    ),
    'group' => t('User'),
    'callbacks' => array(
      'execute' => 'user_badges_taxonomy_user_badge_qualifications_changed',
    ),
  );

  return $items;
}

/**
 * Implements hook_rules_action_info().
 */
function user_badges_taxonomy_rules_action_info() {

  $actions = array();
  $actions['user_badges_taxonomy_update_user_badges'] = array(
    'label' => t('Update User Badges'),
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
      ),
    ),
    'group' => t('User'),
    'callbacks' => array(
      'execute' => 'user_badges_taxonomy_update_user_badges',
    ),
  );
  return $actions;
}

/**
 * Implements rules condition 'user_badges_taxonomy_user_qualifies_for_badge'.
 *
 * @param object $account
 *   The user account.
 *
 * @return bool
 *   TRUE if qualifies; FALSE otherwise.
 */
function user_badges_taxonomy_user_qualifies_for_badge($account) {

  $return = FALSE;
  $roles_badges_map = user_badges_taxonomy_get_roles_badges_map();

  foreach ($account->roles as $rid => $role_name) {
    if (is_numeric($role_name) || $role_name === TRUE) {
      $role = user_role_load($rid);
      $role_name = $role->name;
    }
    if (isset($roles_badges_map[$role_name])) {
      $return = TRUE;
      break;
    }
  }

  return $return;
}

/**
 * Implements rules condition 'User roles changed'.
 *
 * @param object $account
 *   The updated user account.
 * @param object $account_unchanged
 *   User account before update.
 *
 * @return bool
 *   TRUE if changed; FALSE otherwise.
 */
function user_badges_taxonomy_user_badge_qualifications_changed($account, $account_unchanged) {

  $new_roles = $account->roles;
  foreach ($new_roles as $rid => $role_name) {
    if (is_numeric($role_name) || $role_name === TRUE) {
      $role = user_role_load($rid);
      $new_roles[$rid] = $role->name;
    }
  }
  ksort($new_roles);
  // We have to use array_diff both ways,
  // otherwise we miss either added or removed roles:
  $changes = array_merge(array_diff($new_roles, $account_unchanged->roles), array_diff($account_unchanged->roles, $new_roles));

  if (!empty($changes)) {
    return TRUE;
  }

  return FALSE;
}
