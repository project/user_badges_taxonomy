<?php

/**
 * @file
 * User Badges Taxonomy rules integration file.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function user_badges_taxonomy_default_rules_configuration() {

  $rules = array();
  // Add a reaction rule to assign User Badges based on roles
  // for the newly created user.
  $rule = rules_reaction_rule();

  $rule->label = t('When a new user created, assign User Badges based on roles.');
  $rule->active = TRUE;

  $rule
    ->event('user_insert')
    ->condition('user_badges_taxonomy_user_qualifies_for_badge', array(
      'account:select' => 'account',
    ))
    ->action('user_badges_taxonomy_update_user_badges', array(
      'account:select' => 'account',
    ));

  $rules['user_badges_taxonomy_update_user_badges_on_insert'] = $rule;

  // Add a reaction rule to reassign User Badges based on
  // changed roles for the updated user.
  $rule = rules_reaction_rule();

  $rule->label = t('When a user account updated, assign User Badges based on changed roles.');
  $rule->active = TRUE;

  $rule
    ->event('user_update')
    ->condition('user_badges_taxonomy_user_badge_qualifications_changed', array(
      'account:select' => 'account',
      'account_unchanged:select' => 'account_unchanged',
    ))
    ->action('user_badges_taxonomy_update_user_badges', array(
      'account:select' => 'account',
    ));

  $rules['user_badges_taxonomy_update_user_badges_on_update'] = $rule;

  return $rules;
}
